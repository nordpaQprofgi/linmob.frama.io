+++
title = "A910 section at MotorolaFans.com"
aliases = ["2008/08/a910-section-at-motorolafanscom.html"]
date = "2008-08-05T16:12:00Z"
[taxonomies]
categories = ["projects"]
tags = ["A910", "ezx_flexbit.cfg", "modding", "One_A120", "shell scripting", "software"]
authors = ["peter"]
+++
I didn't write here for quite a long time&mdash;I didn't feel like writing about Linux on mobile devices&mdash;and: When there are no real news, why should I write something? Ok, my VIA-based Mini Note / Netbook is there and it is nice, though I wasn't able to add the touchscreen to it yet, as the onboard-usb has weird color codings and I don't want to brake it.
<!-- more -->
Since a few hours ago there is finally a A910 section on Motorolafans&mdash;that's great. But as there was an enourmous (in relation to before) amount of A910s being sold at ebay.co.uk the last week, I believe that it is a good idea to open a new section for a relatively old device. I just got my self a third one, so that I will have two A910s&mdash;one to use and one to &#8220;mod&#8221;.

This means: I'll go on with firmware modding, and I hope to release a new version of my modified A910 firmware in about two to four weeks. If you want to help me with this&mdash;please contact me&mdash;I would like to have somebody, who is able to work on shell scripts (not to complicated, but as i have no experience you could save me a lot of time.

Additionally I have been thinking of creating a wiki which documents Motorolas (A910 or EZX) firmwares lately, lists known bytes of ezx_flexbit.cfg and so on. If anybody has the time (and webspace) to do so, I would be very grateful, as I believe that such a wiki could become quite useful.

And if anybody has got ideas how to get this Motorola firmware to run faster (without overclocking, I tried some APMDs for RoKR E2 and they drained the battery in no time (and the phone was way too hot)), please, again: Contact me, please!
