+++
title = "Re-Evaluating Priorities"
date = "2022-09-03T08:52:00Z"
draft = false
[taxonomies]
tags = ["future",]
categories = ["personal","internal",]
authors = ["peter"]
+++

Did you notice that there's been a countdown on the recent [Weekly Updates](https://linmob.net/categories/weekly-update/)? If so, this post may not really surprise you all that much. Please read it anyway :-)
<!-- more -->

### Boredom and Scarcity of Time
If you’ve been reading the Weekly Updates on this blog for a while, you may remember that I changed the name earlier this year, and tried to change things up a little, because I felt a bit bored and hoped that I would be able to change these regular posts into something that would feel more fun again and less of a chore.

I did not really manage to pull that off. In addition to that, the job change that temporarily coincided with the name change, has made my life a lot more busy, leaving less time and resources to doing fun stuff, to experiment with Linux phones and so on.

Thus, I not only felt increasingly bored by the task of collecting a link list, but also, the thought of “What else could I do with these three to four hours?” has become increasingly hard to shut down. Therefore, I pondered quitting after 128 weekly updates to the end of this year, which is the number the countdown is counting towards.

### Current Events and the Near Future

Now, there’s been another development this week: A close family member who seemed to had recovered from a bad illness is sadly ill again. This changes things – it’s now not just about “Would this time better spent on LinuxPhoneApps.org/writing a how-to/doing $thing” it’s also “Well, I maybe should rather interact with/do something for this person”.

That’s not the only thing. With increased stress levels, I really need to reduce my social media use, as it simply does not make me feel better overall and also likely eats an uncomfortably large amount of time. This includes no longer trying to follow Reddit and some forums via feeds,[^1] and using less of Mastodon and Twitter. (I might even drop Twitter and Mastodon almost entirely, and try follow a handful relevant accounts via RSS.)

I am also tired of performing the same searches over and over on video platforms - I’ve considered finding some “turn search into feed” solution there too, but honestly, the way one video I included in the latest Update ruined a half a day for me, I am not sure if this is really a good idea.

### What Do You Think?

Let’s assume I’ll do all that, is a Weekly Update still with dropping such a large number of sources still a feasible thing, that contains enough info to be worth it? I, personally, am not sure. Now, there’s are other options I thought about:
* How about putting a link to e.g., a hedgedoc document into the bottom of each Weekly Update and ask for contributions of links to noteworthy things? Would that work? Would you contribute?
* Would you maybe even willing to join the effort and "do the chore" every once in a while?

Please let me know, preferably via [email](mailto:peter@linmob.net) or in the [linked hedgedoc](https://pad.hacc.space/5RNpcvFoSRqLYGO39A6Kag#), as I am writing this on the way to visit the aforementioned person and don't plan to be active on social media before Monday.

[^1]: If you're curious about what I am following, here's my current [feeds.opml](https://linmob.uber.space/feeds.opml).
