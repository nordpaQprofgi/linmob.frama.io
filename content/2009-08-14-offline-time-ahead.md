+++
title = "Offline time ahead!"
aliases = ["2009/08/14/offline-time-ahead"]
author = "peter"
date = "2009-08-14T16:07:00Z"
layout = "post"
[taxonomies]
tags = ["open hardware", "gta02-core", "openmoko", "Qi Hardware"]
categories = ["hardware", "commentary"]
authors = ["peter"]
+++

Before I leave the internet alone for some time (as I will go on a long trip to Skagen, Denmark by foot, sleeping in a small tent, no more being connected to the web after passing the danish border) I want to share my thoughts and hopes again by giving a short update on the hardware side open mobile world ;)

This blog is and has always been about Linux on mobile devices and while I didn't care that much about openness and free software, when I started it, I certainly do now. 

The first question to ask is &#8216;Where are we now?&#8217;, and a GPS won't help much to find the location - so one has to try to find it out in other ways. First of all, there is Openmoko. 
Openmoko on the one hand still appears to be alive, as their webservers are still up and running, but one the other hand there weren't any press releases recently and I personally really await the announcement of &#8220;Project b&#8221;  though I am still unsure what it is (besides it is (was?!) a touchscreen device) and when (or even of) it will be announced - probably soon, as Steve Mosher, back then VP of Marketing at Openmoko, suggested in a video in April.

Back to Mr Mosher, who is now CEO of a new start up called "Qi Hardware", that aims to build open or say copyleft hardware on a community driven roadmap. While this approach sounds promising, it will be hard to find a way which is not that difficult to go (makes sense from engineering and hardware support standpoints) but still satisfies the expectations and wishes of the community, which tends to want fast (or even outstanding hardware) which competes well to mass market hardware available in town with the possibility of an anti vendor port. 
This challenge will be tough, I think due to more and more Linux powered devices making their way to mass market, which should make anti vendor ports (thinking of great frameworks like FSO and nice community driven distributions like SHR) less difficult. 
And we shouldn't forget the other challenge which is given through the open roadmap, some people will just wait for the next device, as it will be even better - the fate of Qi Hardware sort of depends on enthusiasts that are interesting in every single piece of Hardware they put on sale.
I could go on with sharing my thoughts on the choice of the SoC, the Ingenic Jz4720, but I won't do that now, maybe later, when I am back.

Having mentionned two of the &#8220;Openmoko forks&#8221; (FSO (which is now a GbR) and Qi Hardware, it would be unfair not to name the third, which is the gta02-core project, which is trying to do community driven hardware design as well and currently appears to be on the way to not doing a &#8216;glamoectomy&#8217;, but as well a Calypso-replacement - at least there is some ongoing discussion on continuing to use the TI made GSM chipset.

Actually, there is always a lot of stuff going on, and it becomes even more if you watch it less closely somehow. I am thrilled to see what will be out when I am back from my trip (which will take approx. 2-4 weeks, depending on my condition and the weather).
