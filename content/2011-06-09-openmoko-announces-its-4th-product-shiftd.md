+++
title = "Openmoko announce their 4th product: Shiftd"
aliases = ["2011/06/openmoko-announces-its-4th-product.html"]
date = "2011-06-09T17:40:00Z"
[taxonomies]
tags = ["announcement", "open source", "openmoko", "shiftd"]
categories = ["software"]
authors = ["peter"]
+++
Today I found this in my inbox. Openmokos next project will be a web application called <a href="http://shiftd.com/">shiftd.com</a> which actually seems quite nice to me. It still has some glitches, and I can't tell whether it will take off&mdash;but it's a nice idea.
<!-- more -->
_(I am pretty sure though, that almost everybody who's subscribed to the Openmoko announce mailing list would be more exited if Openmoko had announced hardware, preferably a wonderfully innovative open tablet or smartphone product.)_

<blockquote>Dear Community! 

Today I get to do one of the things I love most about my job; announce our next product. This time, it's very different from what we've built in the past. No circuit boards were printed. Steel tooling wasn't cut. Mass production didn't dent our view of reality. No. This time, ones and zeros were all it took to assembly Openmoko's fourth product: shiftd.com - A web service to bookmark, share, and discover videos worth watching. 

Like all our previous products, Shiftd started off by scratching a personal itch. We were fascinated, yet totally overwhelmed by the shear volume of videos on the web. We desperately wanted a way to speed up the process of discovering what's worth watching. Existing tools left us deeply unsatisfied. So we set out to build our own. 

Currently, we're focused on Shiftd's core interaction model: bookmarking, sharing, and recommending videos. We have a working prototype. We're excited about using it ourselves, but we know it's far from perfect. Like the Neo 1973 many years ago, I want to share our perspective with you at the earliest possible stage. 

Longterm, our goal is to bring Shiftd to many different types of devices and systems. At this point, technically, we have built only a website, supporting a few videos sites, using Flash not HTML5 video (yet). We have rough ideas for future improvements, including which interfaces to open, but no concrete steps have been taken. We are at the beginning - the time at which we know the least about the project. Purposefully, we have made the fewest binding decisions possible, while still maintaining our original vision. 

Your feedback is critical for us to get this product right. We want Shiftd's heart to beat from the living process that emerges from the journey we take together. Your stories, your real reactions, will intimately grow Shiftd into something great. 

Sign up today at <a href="http://shiftd.com">http://shiftd.com</a>. Start shifting. Tell us what you like and what you don't like. Personally, I'm really looking forward to receiving your recommendations (@mosko) and sharing some of my own favorites with you. 

Sincerely, 

Sean Moss-Pultz 
</blockquote> 

__SOURCE:__ <a href="http://lists.openmoko.org/pipermail/announce/2011-June/000037.html">Openmoko Announce Mailing List</a>
