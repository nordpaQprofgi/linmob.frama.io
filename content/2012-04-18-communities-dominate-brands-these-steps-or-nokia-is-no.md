+++
title = "Communities Dominate Brands: These Steps or Nokia is No More - the way back to profits and growth"
aliases = ["2012/04/18/communities-dominate-brands-these-steps-or-nokia-is-no.html"]
author = "peter"
comments = true
date = "2012-04-18T19:17:00Z"
layout = "post"
[taxonomies]
tags = ["brands", "business", "communities", "linux", "Maemo", "MeeGo", "Meltemi", "microsoft", "Nokia", "nokia n9", "Steven Elop", "Windows Phone"]
categories = ["shortform"]
authors = ["peter"]
+++
<a href='http://communities-dominate.blogs.com/brands/2012/04/these-steps-or-nokia-is-no-more-the-way-back-to-profits-and-growth.html'>Communities Dominate Brands: These Steps or Nokia is No More - the way back to profits and growth</a><div class="link_description"><p>A lengthy, insightful and wishful article on Nokia and how Nokia could manage a turn around. Of course this has to do with mobile Linux, but I don&#8217;t want to spoil it&#8230;</p></div>
