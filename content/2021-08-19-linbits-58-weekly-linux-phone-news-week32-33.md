+++
title = "LinBits 58: Weekly Linux Phone news / media roundup (week 32/33)"
date = "2021-08-19T08:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","LINMOBapps","LinuxPhoneApps","PINE64 Community Update","SupernovaOS","NemoMobile","PineNote","PinePhone accessories","JingOS","JingPad","Ubuntu Touch",]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's ... um... Thursday. Now what happened since last Wednesday?_

A new open source modem firmware release, Maui apps 2.0, NemoMobile progress and more!<!-- more --> _Commentary in italics._ 

### Software releases
* Biktorgj has relaased version [0.3.0 of their open source modem firmware](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.3.0), featuring many bugfixes and an easier way to install it. _Runs fine for me so far!_


### Worth noting
* Chris (kop316), who previously worked on MMS and Visual Voicemails, is now working on another thing that's particularily important in the US: [Spam call protection](https://fosstodon.org/@kop316/106772127260896896).
* HomebrewXS [have announced](https://twitter.com/HomebrewXS/status/1428154290544185345) a release date for their SupernovaOS distribution.
* If you are on Mastodon, [maybe vote on this interesting pull request to mobile-config-firefox](https://fosstodon.org/@ollieparanoid/106762454338839918) that moves the URL and [tab bar to the bottom](https://fosstodon.org/@linmob/106767656106961347)! 

### Worth reading

#### PINE64 Community update
* PINE64: [Introducing the PineNote](https://www.pine64.org/2021/08/15/introducing-the-pinenote/). _This is an impressive update. I really look forward to the PinePhone accessories, but will likely not get into the first batch of PineNote - make sure to read the caveats carefully before you order, if you're not looking for development work._
  * Linux Smartphones: [Pine64 news roundup: PinePhone Keyboard goes on sale in September, Ubuntu Touch 20.04 is coming, visual voicemail arrives, PineNote E Ink tablet revealed](https://linuxsmartphones.com/pine64-news-roundup-pinephone-keyboard-goes-on-sale-in-september-ubuntu-touch-20-04-is-coming-and-visual-voicemail-arrives/).
  * PineGuild: [PineNote: a high-end e-ink device powered by the same SoC as Quartz64](https://pineguild.com/pinenote-a-high-end-e-ink-device-powered-by-the-same-soc-as-quartz64/). 


#### Apps
* Gamey: [Podcasts on the Pinephone & Librem5 | Best podcast clients for GNU/Linux phones!](https://open.lbry.com/@gamey:c/podcasts-on-the-pinephone:8?r=D23fWA6ePDwmZpXPfaLBzkDVj5qWRWYW). _Nice roundup!_
* Claudio Cambra: [Big fixes, more customisation, many improvements — Kalendar week 10 (GSoC 2021)](https://claudiocambra.com/2021/08/14/big-fixes-more-customisation-many-improvements-kalendar-week-10-gsoc-2021/). 
* Sophie Herold: [Get set: Apps for GNOME on its mark](https://blogs.gnome.org/sophieh/2021/08/15/apps-for-gnome-on-its-mark/). _I wish I were this quick with linuxphonapps.org!_
* NxOS: [Maui 2 Release(](https://nxos.org/maui/maui-2-release/). _A truly great release._
  * Linux Smartphones: [Maui 2 released (open source, cross-platform app UI framework)](https://linuxsmartphones.com/maui-2-released-open-source-cross-platform-app-ui-framework/).

#### Distributions 
* Jozef Mlich: [Nemomobile in August/2021](https://blog.mlich.cz/2021/08/nemomobile-in-august-2021/). _Among other things, the boot time has been improved, which should also ease working on the project. There's also a new image, so grab it while it's hot!_
  
#### How to
* CNX Software: [How to use PinePhone as a mobile hotspot](https://www.cnx-software.com/2021/08/08/how-to-use-pinephone-as-a-mobile-hotspot/). _I missed this earlier, maybe it's helpful!_

#### Jing Pad
* TuxPhones: [JingPad A1 (DVT) review, part 2](https://tuxphones.com/jingpad-a1-linux-tablet-review-part-2/). _Great 2nd part of this review. 


### Worth watching

#### PINE64 Community Update
* PINE64: [August Update: Introducing The Pine Note](https://www.youtube.com/watch?v=AwMKfQtSXPE). _Another great video by PizzaLovingNerd!_

#### Bugs
* Drew Naylor 2: [Bug demonstration: Only the most recently-opened Avalonia app is responsive on Phosh](https://www.youtube.com/watch?v=JMHEJ3C5J4k).

#### Howto Privacy
* (RTP) Privacy & Tech Tips: [Howto: Automate Encrypting sdcards + USBstick Hardware Keys (crypto_homes)](https://www.youtube.com/watch?v=Z9txf11RCt0).

#### Waydroid
*  PanzerSajt: [PinePhone running Minecraft Bedrock Edition using WayDroid on Arch Linux](https://www.youtube.com/watch?v=Pe6uVKRBbCM). _This is running better than I would have expected (and than would have ever been possible with old Anbox). Very nice!_ 
* 蕭博文powen: [Waydroid on my pinephone (arch Linux)](https://www.youtube.com/watch?v=OG0BWvwGlk0)
* 蕭博文powen: [It’s my iPhone..android phone...,NO,it’s pinephone](https://www.youtube.com/watch?v=FV2kOIyPHkM). _Funny!_

#### JingOS
* TechHut: [JingOS 0.9.1 on the JingPad A1 - Hands on Walkthrough](https://www.youtube.com/watch?v=xCNWZkrbZ4Y). _Nice!_

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 106](https://www.youtube.com/watch?v=dTiZL-LhFrk). _Florian and Alfred this time, talking about progress and discussing questions. They also ask for donations, as money could help in speeding up the progress of the 20.04 migration. So if you can spare a few bucks, please do! Investing in software really matters!_



### Stuff I did

#### Content

In the past week I only managed to apply minor updates the Resources page and my PinePhone Modem Firmware howto, which is now deprecated.

#### Random

* LinuxPhoneApps.org has made some progress, [enough for an early demo to go live](https://alpha.linuxphoneapps.org). There's still a lot to be done and figure out: Design work (the [app overview](https://alpha.linuxphoneapps.org/apps/) is useless right now), finalizing the nature of app entries, better visibility of and finalizing of taxonomy (e.g. category, UI framework, build system, license) and giving more context on those overview pages. Once that is done, moving the apps over will be a massive task. If you have feedback, please best open an issue on the [GitHub](https://github.com/linuxphoneapps/) project - and if you want to help, get in touch and feel free to open Pull Requests! As I am now on vacation, I likely won't make much progress over the next two weeks, but I should be able to comment on issues and test and merge PRs.
* [Aside from adding a few PKGBUILDs, I worked a bit on the README of my PKGBUILDs repo](https://framagit.org/linmobapps/pkgbuilds).

#### LINMOBapps

The following app was added in the past week, pushing the App count to 310:

* [Telly Scout](https://github.com/plata/telly-scout), a convergent TV Guide based on Kirigami. _Screenshots of [Telly Scout are up on alpha.linuxphoneapps.org](https://alpha.linuxphoneapps.org/apps/org-kde-telly-scout/)!_

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master), as I did some maintenance on other app listings. As with LinuxPhoneApps.org, progress with LINMOBapps will likely be even slower in the coming two weeks, but I will happily accept Merge Requests! So please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
