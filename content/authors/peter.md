+++
title = "Peter" 
+++


### Who am I?

I am Peter, 37, and have been following Linux on Smartphones for quite some time. My day job has nothing to do with Linux. I have a business diploma and my thesis discussed whether a certain crypto currency could have a "fundamental value".

If you want to learn more about me, maybe give the [first episode of the PineTalk podcast](https://www.pine64.org/2021/01/27/001-introduction/) a listen.
<details>
<summary>Disclosures</summary>

I was a co-host for thirteen episodes on the then  bi-weekly [PineTalk Podcast](https://www.pine64.org/pinetalk), a PINE64 community podcast, from January to July 2021. This was pro-bono community work and it should not affect my view on the PINE64 products or competing Linux Phones/handheld devices in any way.
</details>

<details>
<summary>Why do I do this?</summary>
After first trying Linux in the early 2000s, I switched to mostly using Linux around 2005. I was just amazed by all the great Free Software tools. During these years, mobile phones became more interesting: Cameras, colored displays and Smartphones. 

Of course I wanted to have all the great tools of my Linux desktop on my phone, too![^1] Unfortunately, Motorola's EZX phones only shipped a very few native apps. That's why I was very excited when I first heard of OpenMoko.

Unfortunately, OpenMoko eventually failed, unable to ship GTA03, which would have been a massive improvement, fixing most of the flaws of the previous devices. Also, Android took off. Back then, Google felt less problematic to me, and I was glad that it was Android that managed to succeed and not the less user-friendly alternatives like LiMo (which eventually became Tizen) or evil Microsoft's Windows Phone.

Purism's Librem 5 got me really excited when I heard the first rumors before announcement. I backed it in October 2017, and unfortunately Purism's time frame turned out to have been overly optimistic. PINE64 managed to ship their PinePhone earlier, and when I received my first PinePhone I immediately knew that I had to create content about this project and revive this blog.

First: I am not in this for the money. I do not expect to make even one     Dollar or Euro out of this project – I have a day-job, that has nothing     to do with what I am working on here, which pays well enough that I can     spend my free time and some money on this project.[^2]


__TL,DR:__ linmob.net is a passion project!


[^1]: Bad J2ME app support and horrible platforms like Qualcomm's BREW on BenQ/Siemens phones contributed to this. 

[^2]: If you would like to hire me for a job that is closer to what I am working here, that would still allow me to continue with this, feel free to get in touch.

</details>

