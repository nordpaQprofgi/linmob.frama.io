+++
title = "CeBIT starting tomorrow"
aliases = ["2008/03/cebit-starting-tomorrow.html"]
date = "2008-03-03T18:45:00Z"
[taxonomies]
tags = ["cebit", "CeBIT2k8", ]
categories = ["shortform", "events", "personal",]
authors = ["peter"]
+++
As some of you might now, tomorrow CeBIT in Hanover will start. As I got a free ticket, I am looking forward to visit it  (I'll visit my parents and Hanover is on the way), as I already did in 2002. While my focus back then was on PC hardware like fast and affordable mainboards and stuff, my focus this year will be mobile devices, exspecially those running Linux.

Of course it won't be that interesting&mdash;MWC in Barcelona is much more interesting&#8230;&mdash;but nevertheless I'll try to write up a little CeBIT Mobile Linux Coverage, e.g. I'll have a look at what WNC is showing, HTC will get a visit, too&mdash;of course Asus because of Eee PC.
