+++
title = "PinePhone DDC pt. 1: Maps and Navigation"
aliases = ["2020/07/18/pinephone-daily-driver-challenge-part1-maps-navigation.html"]
author = "Peter"
date = "2020-07-18T19:33:18Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["PinePhone", "Video content", "Daily Driver Challenge", "Mobian", "Maps", "Gnome Maps", "Marble Maps", "PureMaps", "Navit", "FoxtrotGPS", "Gnome Web Apps", "Ktrip", "necessary apps"]
categories = ["howto", "videos"]
+++
I shot and [published a video](https://devtube.dev-wiki.de/videos/watch/59bafeef-9942-4c40-ac39-390995982e5a) ([www.youtube.com](https://www.youtube.com/watch?v=WvGNXBntkp0)) yesterday, which is the second part of my "PinePhone as a daily driver" challenge, in which I am currently just trying to solve the most immidiate issues and figure out the most necessary features I would require to use the [PinePhone][pinephone] as my primary smartphone. This time it is about Maps and Navigation.
<!-- more -->

In the [first part](https://linmob.net/2020/07/09/pinephone-daily-driver-challenge-part0.html) of my PinePhone as a Daily Driver series, I had noticed problems with the Gnome Maps app. I wanted to make this video about productivity and Maps, but as the [Gnome Notes app](https://wiki.gnome.org/Apps/Notes) mysteriously stopped showing my Nextcloud Notes, this is just about Maps and other "where am I" and "how do I get to"-problem solving applications.

First I have another look at _[Gnome Maps](https://wiki.gnome.org/Apps/Maps)_, which is quite short and does not get into many of its features because these are not really usable due to Gnome Maps not being optimised for mobile (yet), I switch desktop environment sides and continue with a KDE app. _Marble Maps_, which is in the Mobian repos, is not even being featured prominently on the [Marble website](https://marble.kde.org/features.php). It does not scale well on Phosh, but has a long history on [Android](https://files.kde.org/marble/downloads/android/) and Nokias Linux smartphones [N9](http://talk.maemo.org/showthread.php?t=82196) and [N900](http://talk.maemo.org/showthread.php?t=67316).

Then I have a rather long and exhaustive look at _Pure Maps_ ([Github](https://github.com/rinigus/pure-maps)), which is not only available for [Sailfish, Nemo Mobile](https://openrepos.net/content/rinigus/pure-maps) and [UBports](https://open-store.io/app/pure-maps.jonnius), but can be installed as a [Flatpak](https://www.flathub.org/apps/details/io.github.rinigus.PureMaps) on every other PinePhone distribution, too. It is quite full-featured and can serve as a simple maps app and also be used for navigation. It does not feature an easy way to use it with offline maps for increased privacy and operation in areas without network access; that requires another software called _[OSM Scout Server](https://github.com/rinigus/osmscout-server)_ to be installed and set up.[^1] The [postmarketOS Wiki](https://wiki.postmarketos.org/wiki/Pure_Maps) claims that the combination of OSM Scout Server and Pure Maps can use up to 2 Gigabytes of RAM, rendering it likely unusable on the PinePhone.

After that, I go on having a short look at two apps that have some history and don't run well on Phosh, _[FoxtrotGPS](https://www.foxtrotgps.org/)_ and _[Navit](https://www.navit-project.org/)_[^2], to then continue with [Gnome Web/Epiphany Web Apps](https://wiki.gnome.org/Apps/Web/Docs/Applications).

Following quick looks at _[Gpredict](http://gpredict.oz9aec.net/)_ and _[Grock](https://flathub.org/apps/details/me.leucoso.Grock)_, I end with another KDE App, _[KTrip](https://kde.org/applications/en/utilities/org.kde.ktrip)_ [(KDE Invent)](https://invent.kde.org/utilities/ktrip), _"a public transport assistant targeted towards mobile Linux and Android"_.

Now go and watch it
* on [Peertube](https://devtube.dev-wiki.de/videos/watch/ad95eb65-64bb-4c7c-83a9-b05a16f3c80d),
* on [LBRY](https://lbry.tv/@linmob:3/pinephone-daily-driver-challenge-maps:3),
* or on [YouTube](https://youtube.com/watch?v=WvGNXBntkp0).

[pinephone]: https://www.pine64.org/pinephone/



[^1]: Unfortunately I only figured this out after the video was done and could not try it out yet.
[^2]: I guess I should have read up more here, too.
