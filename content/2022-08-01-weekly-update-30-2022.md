+++
title = "Weekly GNU-like Mobile Linux Update (30/2022): Ubuntu Touch on FairPhone 4, and, maybe, RISC-V phones in the future!"
date = "2022-08-01T20:37:00Z"
draft = false
[taxonomies]
tags = ["Sailfish OS","PinePhone","Phosh","Ubuntu Touch Q&A","PINE64 Community Update","KDE Eco",]
categories = ["weekly update"]
authors = ["peter"]
+++

Also, more and more apps get ported to GTK4/libadwaita, Phosh 0.20 beta 3, KDE progress and more._
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
* This Week in GNOME: [#54 More Portings](https://thisweek.gnome.org/posts/2022/07/twig-54/). _So much GTK4/libadwaita!_
* DEV Community: [GSoC mid term report for Health](https://dev.to/amankrx/gsoc-mid-term-report-for-health-med).
* Berlin Mini GUADEC 2022 reports: [1](https://adrienplazas.com/blog/2022/07/25/berlin-mini-guadec-2022.html), [2](https://danigm.net/berlin-mini-guadec.html), [3](https://blog.jimmac.eu/2022/Berlin-GUADEC/), [4](https://blogs.gnome.org/aday/2022/07/29/berlin-mini-guadec/), [5](https://blogs.gnome.org/tbernard/2022/07/30/berlin-mini-guadec-2022/). _Sounds like it was a great get-together!_

#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: Lots of work on Discover](https://pointieststick.com/2022/07/29/this-week-in-kde-lots-of-work-on-discover/). _Nice progress!_
* Volker Krause: [June/July in KDE Itinerary](https://www.volkerkrause.eu/2022/07/30/kde-itinerary-june-july-2022.html). _Dito._
* KDE Eco: [Measurement Lab Follow-Up: Sprint Achievements And To-dos - KDE Eco](https://eco.kde.org/blog/2022-07-25-sprint-lab-follow-up/). _Nice and measured._
* frinring: [KF5’s big ramp of deprecations to KF6](https://frinring.wordpress.com/2022/07/28/kf5s-big-ramp-of-deprecations-to-kf6/). _Future!_

#### Sailfish OS
* flypig: [Sailfish Community News, 28th July, Xperia 10 II VoLTE](https://forum.sailfishos.org/t/sailfish-community-news-28th-july-xperia-10-ii-volte/12484).


#### Kernel stuff
* Phoronix: [Linux 5.20 To Support The Qualcomm Snapdragon 8cx Gen3, ThinkPad X13s Arm Laptop](https://www.phoronix.com/news/Linux-5.20-SoCs-8cx-Gen3-Arm). _Moar chips!_
  * Phoronix: [Linux 5.20 Likely To Be Called Linux 6.0](https://www.phoronix.com/news/Linux-5.20-Is-Linux-6.0). _Bigger number!_

#### Distributions
* [Release Arch Linux ARM - 2022/07/29 · dreemurrs-embedded/Pine64-Arch · GitHub](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20220729). _New DanctNIX images!_

### Worth noting
* [Guido Günther: "phosh 0.20.0~beta3 is out 🚀📱 : It fixes many b…" - Librem Social](https://social.librem.one/@agx/108737404865743353)
* [Chris: "vvmd version 0.10 is out! https://gitlab.com/kop…" - Fosstodon](https://fosstodon.org/@kop316/108748942741726169)
* [Guido Günther: "Improved #phosh's screenshot support a bit so it …" - Librem Social](https://social.librem.one/@agx/108736883530802491)

### Worth reading

#### PINE64 Community Update
* PINE64: [July Update: A Pinecil Evolved](https://www.pine64.org/2022/07/28/july-update-a-pinecil-evolved/). _No PinePhone news, but don't miss reading the section about the Star64 SBC:_
> "The initial review has yielded some very positive results, partly because the SoC runs cool without the need for passive or active heat dissipation, even under load. The SoC running cool without a heatsink is great news, as it opens the door for the platform to become a basis for future devices."

_StarPhone when? More seriously and totally apart from potential future news in devices, I am quite glad that PINE64 went with StarFive, a well known and decent manufacturer in the RISC-V space (and not AllWinner or worse)._

#### PINE64 Criticism
* TuxPhones: [The Pine Formula](https://tuxphones.com/pine-formula/). _I also scratched my head when I read that PinePod would be based on the same chip as the wireless earbuds. I get that embedded development may be fun; but is it sustainable on chips that are so limited?_

#### Code Hosting
* postmarketOS: [Considering SourceHut](https://postmarketos.org/blog/2022/07/25/considering-sourcehut/). _I don't have a lot of opinions on this; but I really hope that SourceHut add some more lean WebUI for MRs before this happens. While git-send-email is a workflow used by popular and large projects, it's not exactly beginner friendly._

#### Supply Chain
* Purism: [The Ball and Supply Chain Part 2](https://puri.sm/posts/the-ball-and-supply-chain-part-2/).

#### Something to keep in mind
* [Four workers arrive at a construction site ...](https://blogs.kde.org/2022/07/25/four-workers-arrive-construction-site).

### Worth listening
* [postmarketOS podcast // #20 Considering SourceHut Special](https://cast.postmarketos.org/episode/20-Considering-SourceHut-special/). _Great episode!_

### Worth watching

#### Ubuntu Touch Q&A
* UBports: [Ubuntu Touch Q&A 120](https://www.youtube.com/watch?v=JyLtqadrpSY). _They wanted to do a Q&A, so they did!_

#### PinePhone
* Arbitrary Tech: [Pinephone Usability Review Q3 2022](https://www.youtube.com/watch?v=7aDHpPOsuQo) _I bet it's ... 5:21. Also: Don't read the comments._
* Linux Rambling productions: [Some of my thoughts on the pinephone](https://www.youtube.com/watch?v=XGuFFq9Eu1A).
* agisga: [Org-Roam on PinePhone (postmarketOS Linux, SXMO, Emacs 28.1)](https://www.youtube.com/watch?v=D89GjOWYyxY). _Nice!_

### Something missing?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

15
